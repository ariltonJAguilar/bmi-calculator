# BMI Calculator

This Flutter project serves as an example for a form with validation and feedback in the input fields.  

# Sample screens
![enter image description here](https://gitlab.com/ariltonJAguilar/bmi-calculator/-/raw/620c2790971edc3818d75e8b92eae5241279c727/samples/sample2.jpg)

![enter image description here](https://gitlab.com/ariltonJAguilar/bmi-calculator/-/raw/master/samples/sample3.jpg)

![enter image description here](https://gitlab.com/ariltonJAguilar/bmi-calculator/-/raw/master/samples/sample1.jpg)