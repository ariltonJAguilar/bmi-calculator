import 'dart:math';

import 'package:flutter/material.dart';

main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

class DismissKeyboardOnScroll extends StatelessWidget {
  final Widget child;
  final Function onDismiss;

  const DismissKeyboardOnScroll({Key key, this.child, this.onDismiss})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollStartNotification>(
      onNotification: (x) {
        if (x.dragDetails == null) {
          return;
        }

        FocusScope.of(context).unfocus();
        if (onDismiss != null) {
          onDismiss();
        }
      },
      child: child,
    );
  }
}

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  String _informationText = "Informe seus dados!";

  //Validando dados do form
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void calculate() {
    double weight = double.parse(weightController.text);
    double height = double.parse(heightController.text) / 100;
    double imc = weight / pow(height, 2);
    setState(() {
      if (imc < 18.6) {
        //numero de casas decimais
        _informationText = "Abaixo do peso\n${imc.toStringAsPrecision(4)}";
      } else if (imc >= 18.6 && imc < 24.9) {
        _informationText = "Peso ideal\n${imc.toStringAsPrecision(4)}";
      } else if (imc >= 24.9 && imc < 29.9) {
        _informationText =
            "Levemente acima do peso\n${imc.toStringAsPrecision(4)}";
      } else if (imc >= 29.9 && imc < 34.9) {
        _informationText = "Obesidade Grau I\n${imc.toStringAsPrecision(4)}";
      } else if (imc >= 34.9 && imc < 39.9) {
        _informationText = "Obesidade Grau II\n${imc.toStringAsPrecision(4)}";
      } else if (imc >= 40) {
        _informationText = "Obesidade Grau III\n${imc.toStringAsPrecision(4)}";
      }
    });
  }

  void _resetFields() {
    weightController.clear();
    heightController.clear();
    setState(() {
      _informationText = "Informe seus dados!";
      _formKey = GlobalKey<FormState>();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculadora de IMC"),
        centerTitle: true,
        backgroundColor: Colors.green,
        actions: [
          IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                _resetFields();
              })
        ],
      ),
      backgroundColor: Colors.white,
      // Para mover com o teclado, precisa do singelChildScrollView
      body: DismissKeyboardOnScroll(
        child: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Icon(
                    Icons.person_outline_rounded,
                    color: Colors.green,
                    size: 120,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Insira seu Peso!";
                      }
                    },
                    controller: weightController,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        //Mudar cor da borda
                        focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.green, width: 2)),
                        enabledBorder: new UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.green)),
                        labelText: "Peso (kg)",
                        labelStyle: TextStyle(color: Colors.green)),
                    style: TextStyle(color: Colors.green, fontSize: 25),
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Insira sua Altura!";
                      }
                    },
                    controller: heightController,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.green, width: 2)),
                        enabledBorder: new UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.green)),
                        focusColor: Colors.green,
                        labelText: "Altura (cm)",
                        labelStyle: TextStyle(color: Colors.green)),
                    style: TextStyle(color: Colors.green, fontSize: 25),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    height: 50,
                    child: RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          calculate();
                        }
                      },
                      color: Colors.green,
                      child: Text(
                        "Calcular",
                        style: TextStyle(fontSize: 25),
                      ),
                      textColor: Colors.white,
                    ),
                  ),
                  Text(
                    _informationText,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.green, fontSize: 25),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
